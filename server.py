""" Server for messenger """
from socket import socket, AF_INET, SOCK_STREAM
from threading import Thread
from db import db_helper
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

clients = {}
addresses = {}

HOST = ''
PORT = 3338
BUFFER_SIZE = 1024
ADDR = (HOST, PORT)
SERVER = socket(AF_INET, SOCK_STREAM) #TCP
SERVER.bind(ADDR)


#   implement via SMTP-> Simple Mail Transfer Protocol
def send_mail(username, email):
    try:
        smtp_server = smtplib.SMTP('localhost', 25)
        sender = 'localhost'
        receiver = email

        msg = MIMEMultipart()
        msg['From'] = sender
        msg['To'] = receiver
        msg['Subject'] = "TEST"
        body = "Hello {0}, a sample test message".format(username)
        msg.attach(MIMEText(body, 'plain'))
        text = msg.as_string()

        smtp_server.sendmail(sender, receiver, text)
        print("successfully sent")
        return True
    except smtplib.SMTPException:
        print("not sent")
        return False


def accept_connections():
    while True:
        client, client_address = SERVER.accept()
        print("{0} has connected.".format(client_address)) #maybe!
        #client.send(bytes("Greeting from Sina\nNow type your name and press enter", "utf8"))
        client.send(bytes("SignIn (0) or SingUp (1) ? (0/1)", "utf8"))
        addresses[client] = client_address
        Thread(target=handle_client, args=(client,)).start() #Threads in Python

def send_to(client_socket, message):
    client_socket.send(bytes(message, "utf8"))

def get_received_from(client_socket):
    return client_socket.recv(BUFFER_SIZE).decode("utf8")

def handle_client(client): # argument: client socket
    """ single client connection handling """
    #name = client.recv(BUFFER_SIZE).decode("utf8")
    username =' '
    password=' '
    sign_in_up = get_received_from(client)
    print(sign_in_up)
    if sign_in_up == '0':
        # SignIn
        while True:
            send_to(client, "Please enter username: ")
            username = get_received_from(client)
            send_to(client, "Please enter your password: ")
            password = get_received_from(client)
            if db_helper.sign_in((username, password)) is True:
                
                send_to(client, "Welcome {0}!".format(username))
                break
            else:
                send_to(client, "SignIn failed, please try again")
    elif sign_in_up == '1':
        send_to(client, "Please enter username: ")
        username = get_received_from(client)
        send_to(client, "Please enter password: ")
        password = get_received_from(client)
        send_to(client, "Please enter email: ")
        email = get_received_from(client)
        db_helper.sign_up((username, password, '0', email))
        send_mail(username, email)
        send_to(client, "Welcome {0}!".format(username))


    #welcome_message = "Welcome %s !\n for exiting, type {quit} " %name
    #client.send(bytes(welcome_message, "utf8"))
    message = '{0} has joined the chat'.format(username)
    broadcast(bytes(message, 'utf8'))
    client_id = db_helper.get_id((username, password))
    clients[client] = client_id
    while True:
        message = client.recv(BUFFER_SIZE)
        if message != bytes("{quit}", "utf8"):
            broadcast(message, username+': ')
        else:
            client.send(bytes("{quit}", "utf8"))
            client.close()
            del clients[client]
            broadcast(bytes("{0} has left the chat", "utf8"))
            break


def broadcast(message, prefix=""):
    """ send message to all the clients """
    for s in clients:
        s.send(bytes(prefix, "utf8")+message)


def get_all_messages(client_id, contact_id):
    messages = db_helper.get_history((client_id, contact_id))
    return messages


def get_online_users():
    users = db_helper.get_online_users()
    return users


if __name__ == "__main__":
    SERVER.listen(10) # max connection
    print("Waiting for connection ...")
    accepted_thread = Thread(target=accept_connections)
    accepted_thread.start()
    accepted_thread.join() #wait to complete thread and then jump to next line
    SERVER.close()

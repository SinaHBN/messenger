""" client of messenger """

from socket import socket, AF_INET, SOCK_STREAM
from threading import Thread
import tkinter




def receive():
    """ receive messages """
    while True:
        try:
            message = client_socket.recv(BUFFER_SIZE).decode("utf8")
            message_list.insert(tkinter.END, message)
        except OSError: #I/O Error | if client has left the chat
            break


def send(event=None): #event passed by tkinter implicitly # باید حتما بررسی کنم -> به‌نظرم مهمه
    """ send messages """
    message = message_field.get() 
    message_field.set("") # clear input 
    client_socket.send(bytes(message, "utf8"))
    if message == "{quit}":
        client_socket.close()
        tk.quit() # close the gui
    

def on_closing(event=None): # close connection before close gui
    """ call when window is closed """
    message_field.set("{quit}")
    send()


def show_users():
    pass


#______________________________________________________________
#building GUI

tk = tkinter.Tk()
tk.title("Chat App")

message_frame = tkinter.Frame(tk)

message_field = tkinter.StringVar()
message_field.set("Type your message here...")

scrollbar = tkinter.Scrollbar(message_frame)
message_list = tkinter.Listbox(message_frame, height=15, width=50, yscrollcommand=scrollbar.set, bg='#f2f8e4',font='Times')
scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)
message_list.pack(side=tkinter.LEFT, fill=tkinter.BOTH)

message_list.pack()
message_frame.pack()

contact_scrollbar = tkinter.Scrollbar(tk)
contact_list = tkinter.Listbox(tk, height=15, yscrollcommand=scrollbar.set, bg='#80add7',font='Helvetica')
contact_scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)
contact_list.pack(side=tkinter.RIGHT, fill=tkinter.BOTH)


entry_field = tkinter.Entry(tk, textvariable=message_field,font='Lato',width=40)
entry_field.bind("<FocusIn>", lambda args: entry_field.delete('0', 'end'))
entry_field.bind("<Return>", send)
entry_field.pack(side=tkinter.LEFT, padx=10)

send_button = tkinter.Button(tk, text="Send",fg='#003d73',font='Vollkorn')
send_button.pack(side=tkinter.LEFT,fill=tkinter.X)

#broadcast_button = tkinter.Button(tk, text="Broadcast")
#broadcast_button.pack()
HOST = input('Please enter host: ')
PORT = input('Please enter port: ')

if not PORT:
    PORT = 3333
else:
    PORT = int(PORT)

BUFFER_SIZE = 1024
ADDR = (HOST, PORT)
client_socket = socket(AF_INET, SOCK_STREAM)
client_socket.connect(ADDR)


receive_thread = Thread(target=receive)
receive_thread.start()
tkinter.mainloop()

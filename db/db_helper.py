import sqlite3


def connect_to_db():
    connection = sqlite3.connect('db/messenger.db')
    return connection, connection.cursor()


def close_db(connection):
    connection.commit()
    connection.close()


def sign_in(record):
    result = False
    connection, c = connect_to_db()
    c.execute('select * from User where Username = ? and Password = ?', record)
    if c.fetchone() !=None:
        c.execute("update User set Is_online='1' where Username = ? and Password = ?", record)
        
        result = True
    else:
        result = False

    close_db(connection)
    return result
    

def sign_up(record):

    connection, c = connect_to_db()
    c.execute("insert into User (Username, Password, Is_online, Email) values (?,?,?,?)", record)
    close_db(connection)


def show_list_of_online_users():
    connection, c = connect_to_db()
    c.execute("select * from User where Is_online = '1' ")
    close_db(connection)
    return c.fetchall()


def get_id(record):
    connection, c = connect_to_db()
    c.execute("select Id from User where Username = ? and Password = ?", record)
    close_db(connection)
    return c.fetchone()


def logout(record):
    connection, c = connect_to_db()
    c.execute("update User set Is_online='0' where Username = ? and Password = ?", record)
    close_db(connection)


def get_history(record):
    connection, c = connect_to_db()
    c.execute("SELECT 'content' FROM Message WHERE SenderId=? AND ReceiverId=?", record)
    table = c.fetchall()
    close_db(connection)
    return table


def get_online_users():
    connection, c = connect_to_db()
    c.execute("SELECT Id, Username FROM User WHERE Is_online='1'")
    table = c.fetchall()
    close_db(connection)
    return table
